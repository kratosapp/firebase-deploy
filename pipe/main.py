import json
import subprocess
import sys
from io import StringIO
import re

import yaml
from bitbucket_pipes_toolkit import Pipe, get_variable, get_logger
import logging

ansi_escape = re.compile(r'''
    \x1B  # ESC
    (?:   # 7-bit C1 Fe (except CSI)
        [@-Z\\-_]
    |     # or [ for CSI, followed by a control sequence
        \[
        [0-?]*  # Parameter bytes
        [ -/]*  # Intermediate bytes
        [@-~]   # Final byte
    )
''', re.VERBOSE)

def log_subprocess_output(pipe):
    for line in iter(pipe.readline, b''):  # b'\n'-separated lines
        logging.info(line.decode(errors='ignore'))


schema = {
    "FIREBASE_TOKEN": {"required": True, "type": "string"},
    "PROJECT_ID": {"required": False, "type": "string", "nullable": True, "default": None},
    "MESSAGE": {"type": "string", "required": False, "nullable": True, "default": None},
    "EXTRA_ARGS": {"type": "string", "required": False, "default": ''},
    "DEBUG": {"type": "boolean", "required": False, "default": False},
    "RETRY": {"type": "integer", "required": False, "default": 0},
}

logger = get_logger()


class FirebaseDeploy(Pipe):

    def run(self):
        fs = StringIO()
        logger.addHandler(logging.StreamHandler(fs))
        super().run()
        retry = self.get_variable('RETRY')
        logger.info(f'Executing the pipe... Retries: {retry}')
        project = self.get_variable('PROJECT_ID')
        token = self.get_variable('FIREBASE_TOKEN')
        message = self.get_variable('MESSAGE')
        extra_args = self.get_variable('EXTRA_ARGS')
        debug = self.get_variable('DEBUG')
        success = False

        commit = get_variable('BITBUCKET_COMMIT', default='local')
        repo = get_variable('BITBUCKET_REPO_SLUG', default='local')
        workspace = get_variable('BITBUCKET_WORKSPACE', default='local')

        if message is None:
            message = get_variable('MESSAGE', default=f'Deploy {commit} from https://bitbucket.org/{workspace}/{repo}')

        args = [
            'firebase',
            '--token', token,
        ]

        if debug:
            args.append('--debug')

        if project is None:
            # get the project id from .firebaserc
            logger.info('Project id not specified, trying to fectch it from .firebaserc')
            try:
                data = json.load(open('.firebaserc'))
                project = data['projects']['default']
            except FileNotFoundError:
                self.fail(message='.firebaserc file is missing and is required')
            except KeyError:
                self.fail(message='Was not able to find project ID in .firebaserc. Check your configuration.')

        args.extend(['--project', project])
        args.extend(['deploy', '--message', message])

        args.extend(extra_args.split())

        logger.info('Starting deployment of the project to Firebase.')

        process = subprocess.Popen(args,
                                stdout=subprocess.PIPE, 
                                stderr=subprocess.STDOUT
                                )
        with process.stdout:
            log_subprocess_output(process.stdout)
        exitcode = process.wait()
        logging.info(f"EXIT CODE: {exitcode}")
        if exitcode != 0:
            logger.warning('Deployment failed.')
            args_copy = args.copy()
            while retry > 0:
                logger.info(f'Retry {retry}')
                fs.seek(0)
                log = fs.read()
                logger.info(f"Log length  "+ str(len(log)))
                logger.info(f"Log lines  "+ len(log.split('\n')))

                
                logger.info(f'Getting failed functions')
                
                
                logger.info(f"Log lines  "+ str(log.split('\n')[-20:]))
                
                line = list(filter(lambda x: 'firebase deploy --only functions:' in ansi_escape.sub('',x), log.split('\n')))[0]. \
                    replace('b\'', '').replace('\'', '').replace('\\n', '').strip()
                line = ansi_escape.sub('',line)
                logger.info(f'Line: {line}')
                args = args_copy.copy()
                args.extend(['--only', line.replace('firebase deploy --only ', '')])
                logger.info(args)
                process = subprocess.Popen(args,
                                stdout=subprocess.PIPE, 
                                stderr=subprocess.STDOUT
                                )
                with process.stdout:
                    log_subprocess_output(process.stdout)
                exitcode = process.wait()
                logging.info(f"EXIT CODE: {exitcode}")
                if exitcode != 0:
                    retry -= 1
                    fs = StringIO()
                else:
                    retry = 0
                    success = True
                    self.success(
                        f'Successfully deployed project {project}. Project link: https://console.firebase.google.com/project/{project}/overview')
        else:
            success = True
        if not success:
            self.fail(f'Failed to deploy project')


if __name__ == '__main__':
    with open('/usr/bin/pipe.yml', 'r') as metadata_file:
        metadata = yaml.safe_load(metadata_file.read())
    pipe = FirebaseDeploy(pipe_metadata=metadata, schema=schema)
    pipe.run()
